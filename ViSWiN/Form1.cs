﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViSys.Modules.Camera;
using ViSys.Modules.Camera.Common;
using ViSysUtil;

namespace ViSWiN
{
    public partial class Form1 : Form
    {
        public LoadSystemConfiguration systemConfiguration;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                this.systemConfiguration = new LoadSystemConfiguration();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao carregar o arquivo de configuração."
                    + "\n\n"
                    + ex.ToString()
                    + "\n\n"
                    + "A aplicação será encerrada.");

                Application.Exit();
            }

            CameraModuleFactory cameraFactory = new CameraModuleFactory();

            ICameraModule iCameraLeft = cameraFactory.Create(CameraModuleFactory.CameraAPIProvider.SAPERA);
            iCameraLeft.Open("Linea_M8192-7um_1");
            iCameraLeft.GetFramesAsync();
            iCameraLeft.NewFrame += iCameraLeft_NewFrame;

            ICameraModule iCameraRight = cameraFactory.Create(CameraModuleFactory.CameraAPIProvider.SAPERA);
            iCameraRight.Open("Linea_M8192-7um_2");
            iCameraRight.GetFramesAsync();
            iCameraRight.NewFrame += ICameraRight_NewFrame;          

        }

        private void ICameraRight_NewFrame(object sender, SysImage e)
        {
            this.imageBox2.Image = e.ImageMat;
        }

        private void iCameraLeft_NewFrame(object sender, SysImage e)
        {
            this.imageBox1.Image = e.ImageMat;
        }
    }
}
