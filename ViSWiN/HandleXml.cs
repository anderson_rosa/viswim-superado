﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ViSysUtil
{
    class HandleXml
    {
        private XmlDocument doc;
        private string folderName { get; set; }
        private string fileName { get; set; }

        public HandleXml(string folderName, string fileName)
        {
            doc = new XmlDocument();
            this.folderName = folderName;
            this.fileName = fileName;

            string fullFileName = this.folderName + @"\" + this.fileName + @".xml";

            try
            {
                doc.Load(fullFileName);
            }
            catch (Exception ex)
            {
                string errorMessage = "Arquivo xml não encontrado no caminho: " + fullFileName;

                System.Windows.Forms.MessageBox.Show(errorMessage, "Erro");

                throw new Exception(errorMessage, ex);
            }
        }

        public HandleXml(string folderName, string fileName, XmlDocument doc)
        {
            this.doc = doc;
            this.folderName = folderName;
            this.fileName = fileName;
        }

        public string getNodeValue(string nodeName)
        {
            if (doc.GetElementsByTagName(nodeName).Count > 0)
            {
                string a = doc.GetElementsByTagName(nodeName).Item(0).InnerText;
                return doc.GetElementsByTagName(nodeName).Item(0).InnerText;
            }
            return "";
        }

        public string[] getNodeValues(string nodeName)
        {
            string[] values = new string[doc.GetElementsByTagName(nodeName).Count];

            for (int i = 0; i < doc.GetElementsByTagName(nodeName).Count; i++)
            {

                values[i] = doc.GetElementsByTagName(nodeName)[i].InnerText;
            }

            return values;
        }

        public string[,] getNodeValues(string criteria, string[] atributes)
        {
            // Add the namespace.
            //XmlNamespaceManager nsmgr = new XmlNamespaceManager(this.doc.NameTable);
            //nsmgr.AddNamespace("slagNS", @"vislag:slagData-schema");


            XmlNode root = this.doc.GetElementsByTagName("operator")[0];
            //XmlNodeList nodeList = root.SelectNodes("slagNS:" + criteria, nsmgr);
            XmlNodeList nodeList = root.SelectNodes(criteria);
            string[,] values = new string[nodeList.Count, atributes.Length];

            for (int i = 0; i < nodeList.Count; i++)
            {
                for (int j = 0; j < atributes.Length; j++)
                {
                    if (nodeList[i].Attributes.GetNamedItem(atributes[j]) != null)
                    {
                        values[i, j] = nodeList[i].Attributes.GetNamedItem(atributes[j]).InnerText;
                    }
                    else
                    {
                        values[i, j] = "---";
                    }
                }
            }

            return values;
        }

        public string[,] getNodeValuesNoNameSpace(string criteria, string[] atributes)
        {
            XmlNode root = this.doc.GetElementsByTagName("operator")[0];
            XmlNodeList nodeList = root.SelectNodes(criteria);
            string[,] values = new string[nodeList.Count, atributes.Length];

            for (int i = 0; i < nodeList.Count; i++)
            {
                for (int j = 0; j < atributes.Length; j++)
                {
                    values[i, j] = nodeList[i].Attributes.GetNamedItem(atributes[j]).InnerText;
                }
            }

            return values;
        }


        public string getAttributeValue(string nodeName, string attribute)
        {
            return doc.GetElementsByTagName(nodeName).Item(0).Attributes.GetNamedItem(attribute).InnerText;
        }
    }
}


