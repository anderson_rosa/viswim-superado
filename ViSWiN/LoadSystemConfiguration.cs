﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViSysUtil
{
    public class LoadSystemConfiguration
    {
        private HandleXml confXmlHandler;
        string systemPath;

        private string api;
        private string firstCam, secondCam;
        private string firstCamSettingsFilePath, secondCamSettingsFilePath;
        private int camDistanceInMillimeter;
        private double firstCamPixelMillimeter;
        private double secondCamPixelMillimeter;


        public LoadSystemConfiguration()
        {
            this.systemPath = System.IO.Path.GetFullPath(@"..\..\..\");
            this.confXmlHandler = new HandleXml(systemPath + @"Conf", @"configuracaoSistema");

            this.api = confXmlHandler.getNodeValue("api");

            this.firstCam = confXmlHandler.getNodeValue("name-id-1");
            this.firstCamSettingsFilePath = confXmlHandler.getNodeValue("cam-1-settings");
            this.firstCamPixelMillimeter = double.Parse(confXmlHandler.getNodeValue("cam-1-pixel-mm"));

            this.secondCam = confXmlHandler.getNodeValue("name-id-2");
            this.secondCamSettingsFilePath = confXmlHandler.getNodeValue("cam-2-settings");
            this.secondCamPixelMillimeter = double.Parse(confXmlHandler.getNodeValue("cam-2-pixel-mm"));

            this.camDistanceInMillimeter = int.Parse(confXmlHandler.getNodeValue("cam-distance-mm"));
        }

    }
}

